<?php

/** @var \Laravel\Lumen\Routing\Router $router */


use App\Mail\SendMailReset;
use Illuminate\Support\Facades\DB;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/login', 'AuthController@postLogin');
$router->post('/register', 'AuthController@postRegister');



$router->group(['middleware' => 'auth'], function () use ($router){
    $router->get('/dashboard', function (){
        return response()->json(['message'=>'dashboard page']);
    });
    $router->post('/user/send_password_reset_link','PasswordResetRequestController@sendEmail');
});
//$router->post('/password/reset', 'ResetPasswordController@reset');
//$router->get('/users', function()use ($router) {
//    return DB::select("SELECT * FROM users");
//});
//
//$router->get('/password/test', 'ExampleController@test');
//$router->get('/email_template/email', function (){
//    return new SendMailReset('Hello', 'World');
//});
//
